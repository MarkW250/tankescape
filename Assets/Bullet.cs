﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    // seconds until bullet destroys
    private float _timeToLast = 2.0f;
    private float _bulletSpeed = 20.0f;

    public float Damage = 100.0f;

    /* My Objects */
    Rigidbody2D _rigidbody = null;
    Animator _animator = null;

    // Start is called before the first frame update
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();

        _rigidbody.velocity = transform.up * _bulletSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        _timeToLast = _timeToLast - Time.deltaTime;
        if (_timeToLast < 0.0f)
        {
            GameObject.Destroy(this.gameObject);
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        //if ( LayerMask.LayerToName(collision.transform.gameObject.layer) != "NonPhysical")
        //{
            _animator.SetTrigger("explode");
            _rigidbody.velocity = new Vector2();
        //}
    }

    private void OnAnimationExplosionEnd()
    {
        GameObject.Destroy(this.gameObject);
    }
}
