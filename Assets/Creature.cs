﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Creature : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Creature hit points")]
    private float _maxHitPoints = 100.0f;

    [SerializeField]
    [Tooltip("Creature movement speed")]
    private float _moveSpeed = 10.0f;

    [SerializeField]
    [Tooltip("Period between creature attacks (in seconds)")]
    private float _attackDebounce = 1.0f;

    [SerializeField]
    [Tooltip("Time after creature attacks that it can move (in seconds)")]
    private float _moveDebounce = 0.5f;

    [SerializeField]
    [Tooltip("Creature attack damage")]
    private float _attackDamage = 10.0f;

    [SerializeField]
    [Tooltip("Sound to play when walking")]
    private AudioClip _walkingSound = null;

    [SerializeField]
    [Tooltip("Sound to play when dying")]
    private AudioClip _dyingSound = null;

    /* My Objects */
    private Rigidbody2D _rigidBody = null;
    private Animator _animator = null;
    AudioSource _walkingAudioSource;
    AudioSource _dyingAudioSource;

    /* Private state variables */
    private float _attackTimer = 0.0f;
    private float _moveTimer = 0.0f;
    private bool _playerInRange = false;
    private bool _attacking = false;
    private float _hitPoints;
    private bool _dead = false;

    // Start is called before the first frame update
    void Start()
    {
        _hitPoints = _maxHitPoints;
        _rigidBody = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
        _walkingAudioSource = gameObject.GetComponent<AudioSource>();
        _walkingAudioSource.clip = _walkingSound;
        _dyingAudioSource = gameObject.AddComponent<AudioSource>();
        _dyingAudioSource.clip = _dyingSound;
    }

    // Update is called once per frame
    void Update()
    {
        if(_dead)
        {
            return;
        }

        if(PlayerController.IsGamePaused())
        {
            return;
        }

        HandleHealth();
        HandleAttack();
    }

    private void FixedUpdate()
    {
        if (_dead)
        {
            return;
        }

        if (PlayerController.IsGamePaused())
        {
            return;
        }

        HandleMovement();
    }

    private void HandleAttack()
    {
        if(_attackTimer > 0.0f)
        {
            _attackTimer -= Time.deltaTime;
        }
        else
        {
            if (_playerInRange && !_attacking)
            {
                _animator.SetTrigger("attack");
                _attacking = true;
            }
        }
    }

    private void HandleMovement()
    {
        Vector2 newVelocty = new Vector2();

        Vector2 playerPosition = PlayerController.Instance.transform.position;
        Vector2 vectorToPlayer = playerPosition - (Vector2)transform.position;

        if (_moveTimer > 0.0f)
        {
            _moveTimer -= Time.deltaTime;
        }
        else
        {
            if (!_playerInRange && !_attacking)
            {
                newVelocty = vectorToPlayer.normalized * _moveSpeed;
                _rigidBody.AddForce(newVelocty);
            }
        }

        transform.up = vectorToPlayer.normalized;

        //_rigidBody.velocity = newVelocty;

        if(_rigidBody.velocity.magnitude > 0.5f)
        {
            _animator.SetBool("moving", true);
            if(!_walkingAudioSource.isPlaying)
            {
                _walkingAudioSource.Play();
            }
        }
        else
        {
            _animator.SetBool("moving", false);
            _walkingAudioSource.Stop();
        }

    }

    private void HandleHealth()
    {
        if (_hitPoints <= 0.0f)
        {
            die();
            TickSpawner.Instance.OnTickDeath();
        }
    }

    private void die()
    {
        _animator.SetTrigger("death");
        _dyingAudioSource.Play();
        _dead = true;
        gameObject.layer = LayerMask.NameToLayer("Corpse");
    }

    private void OnAnimationEndDeath()
    {
        GameObject.Destroy(this.gameObject);

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        Bullet bullet = other.transform.GetComponent<Bullet>();
        if(bullet != null)
        {
            _hitPoints -= bullet.Damage;
        }

        if (other.gameObject.GetComponent<PlayerController>() != null)
        {
            _playerInRange = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<PlayerController>() != null)
        {
            _playerInRange = false;
        }
    }

    private void OnAnimationAttackFinished()
    {
        _moveTimer = _moveDebounce;
        _attackTimer = _attackDebounce;
        _attacking = false;
        if (_playerInRange)
        {
            PlayerController.Instance.Hurt(_attackDamage);
        }
    }
}
