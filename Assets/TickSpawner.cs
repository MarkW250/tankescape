﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TickSpawner : MonoBehaviour
{
    public static TickSpawner Instance;

    [SerializeField]
    [Tooltip("Tick Prefab")]
    private GameObject _trickPrefab = null;

    [SerializeField]
    [Tooltip("Spawn time in seconds")]
    private float _spawnTime = 10.0f;

    [SerializeField]
    [Tooltip("Max number of ticks on map")]
    private int _maxTicks = 50;

    int _tickCount = 0;
    bool _init = false;

    float _spawnTimer = 0.0f;

    public void OnTickDeath()
    {
        if (_tickCount > 0)
        {
            _tickCount--;
        }
        Debug.Log("Tick Death: New Tick Count: " + _tickCount.ToString());
    }

    // Start is called before the first frame update
    void Start()
    {
        _spawnTimer = _spawnTime;
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            GameObject.Destroy(this.gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(!_init)
        {
            _init = true;
            _tickCount = GameObject.FindObjectsOfType<Creature>().Length;
            Debug.Log("Initial Tick Count: " + _tickCount.ToString());
        }

        if(PlayerController.IsGamePaused())
        {
            return;
        }

        if(_spawnTimer > 0.0f)
        {
            _spawnTimer -= Time.deltaTime;
        }
        else
        {
            if (_tickCount < _maxTicks)
            {
                _spawnTimer = _spawnTime;
                _tickCount++;

                Vector2 spawnPoint;
                bool inCameraView = true;
                int retry = 100;
                do
                {
                    float xSpawn = Random.Range(-transform.localScale.x / 2.0f, transform.localScale.x / 2.0f);
                    float ySpawn = Random.Range(-transform.localScale.y / 2.0f, transform.localScale.y / 2.0f);
                    spawnPoint = new Vector2(xSpawn, ySpawn);

                    Vector2 pointInCamera = Camera.main.WorldToScreenPoint(spawnPoint);
                    if ((pointInCamera.x < 0) || (pointInCamera.x > Camera.main.pixelWidth))
                    {
                        if ((pointInCamera.y < 0) || (pointInCamera.y > Camera.main.pixelHeight))
                        {
                            inCameraView = false;
                        }
                    }
                    retry--;

                } while ((inCameraView) && (retry > 0));

                GameObject.Instantiate(_trickPrefab, spawnPoint, new Quaternion());
                Debug.Log("New Tick: New Tick Count: " + _tickCount.ToString());
            }
        }
    }
}
