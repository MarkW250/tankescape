﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Controller : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Movement Control Sprite")]
    SpriteRenderer _movementControlSprite = null;

    [SerializeField]
    [Tooltip("Turret Control Sprite")]
    SpriteRenderer _turretControlSprite = null;

    [SerializeField]
    [Tooltip("Dark Overlay Object")]
    GameObject _darkOverlay = null;

    [SerializeField]
    [Tooltip("Pause screen object")]
    GameObject _pauseScreen = null;

    [SerializeField]
    [Tooltip("Pause screen object")]
    GameObject _titleScreen = null;

    [SerializeField]
    [Tooltip("Game win screen object")]
    GameObject _gameWinScreen = null;

    [SerializeField]
    [Tooltip("Game Lose screen object")]
    GameObject _gameLoseScreen = null;

    [SerializeField]
    [Tooltip("Movement Cursor Sprite")]
    private Texture2D _cursorMove = null;

    [SerializeField]
    [Tooltip("Movement Cursor Sprite")]
    private Texture2D _cursorTurret = null;


    public void SetController(PlayerController.Control newControl)
    {
        setSpriteActive(_movementControlSprite, false);
        setSpriteActive(_turretControlSprite, false);

#if UNITY_WEBGL
        CursorMode mode = CursorMode.ForceSoftware;
#else
        CursorMode mode = CursorMode.Auto;
#endif

        switch (newControl)
        {
            case PlayerController.Control.Movement:
                setSpriteActive(_movementControlSprite, true);
                Cursor.SetCursor(_cursorMove, new Vector2(0.5f, 0.5f), mode);
                break;

            case PlayerController.Control.Turret:
                setSpriteActive(_turretControlSprite, true);
                Cursor.SetCursor(_cursorTurret, new Vector2(0.5f, 0.5f), mode);
                break;

            default:
                break;
        }
    }


    public void SetPauseScreen(bool active)
    {
        _darkOverlay.SetActive(active);
        _pauseScreen.SetActive(active);
    }

    public void SetTitleScreen(bool active)
    {
        _darkOverlay.SetActive(active);
        _titleScreen.SetActive(active);
    }

    public void SetWinScreen(bool active)
    {
        _darkOverlay.SetActive(active);
        _gameWinScreen.SetActive(active);
    }

    public void SetLoseScreen(bool active)
    {
        _darkOverlay.SetActive(active);
        _gameLoseScreen.SetActive(active);
    }

    void setText(Text text, bool highlighted)
    {
        if(highlighted)
        {
            text.fontStyle = FontStyle.Bold;
            text.fontSize = 18;
        }
        else
        {
            text.fontStyle = FontStyle.Normal;
            text.fontSize = 14;
        }
    }

    void setSpriteActive(SpriteRenderer sprite, bool active)
    {
        if (active)
        {
            sprite.color = Color.green;
        }
        else
        {
            sprite.color = Color.red;
        }
    }



}
