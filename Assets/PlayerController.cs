﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public static PlayerController Instance;

    public static bool IsGamePaused()
    {
        return Instance.IsPaused();
    }

    /* Types */
    public enum Control
    {
        Movement,
        Turret
    }

    /* Editor exposed properties */
    [SerializeField]
    [Tooltip("Movement speed in units per second")]
    private float _moveForce = 10.0f;

    [SerializeField]
    [Tooltip("Tank base rotate speed in degrees per second")]
    private float _baseRotateSpeed = 100.0f;

    [SerializeField]
    [Tooltip("Turret game object")]
    private GameObject _turret = null;

    [SerializeField]
    [Tooltip("Tank turret rotate speed in degrees per second")]
    private float _turretRotateSpeed = 100.0f;

    [SerializeField]
    [Tooltip("Bullet prefab object")]
    private GameObject _bulletPrefab = null;

    [SerializeField]
    [Tooltip("Fire debounce (in seconds)")]
    private float _shootDebounce = 0.5f;

    [SerializeField]
    [Tooltip("Fire debounce (in seconds)")]
    private float _maxHitPoints = 100.0f;

    [SerializeField]
    [Tooltip("Camera object to follow player")]
    private Camera _camera = null;

    [SerializeField]
    [Tooltip("Point of Barrel Exit")]
    private GameObject _barrelExit = null;

    [SerializeField]
    [Tooltip("Sound to play on shoot")]
    private AudioClip _shootSound = null;

    [SerializeField]
    [Tooltip("Sound to play on driving")]
    private AudioClip _drivingSound = null;

    [SerializeField]
    [Tooltip("Sound to play on idling")]
    private AudioClip _idleSound = null;

    [SerializeField]
    [Tooltip("Sound to play on switching control")]
    private AudioClip _switchControlSound = null;

    [SerializeField]
    [Tooltip("Sound to play on tank explosion")]
    private AudioClip _tankExplosionSound = null;

    [SerializeField]
    [Tooltip("Sound to play on tank hit")]
    private AudioClip _tankHitSound = null;

    /* My objects */
    Rigidbody2D _rigidBody = null;
    Animator _animator = null;
    float _hitPoints;
    UI_Controller _uiController = null;
    StatusBar _healthBar = null;
    AudioManager _audioManager;
    AudioSource _drivingAudioSource;

    /* Private settings */
    const float _randomMousePointerTime = 0.1f;             // time to check if we should change virtual mouse pointer
    const float _virtualMoustPointerChangeProb = 0.2f;      // probability that we will change virtual mouse pointer
    const float _randomMovementTime = 0.1f;                 // time to check if we should change the random movement
    const float _randomMovementChangeProb = 0.1f;           // probability that we will toggle moving when not in control
    const float _randomShootTime = 0.1f;                    // time to check if we should rahdomly shoot
    const float _randomShootProb = 0.02f;                    // probability that we will randomly shoot

    /* Private state variables */
    Control _currentControl = Control.Movement;
    float _shootTimer = 0.0f;
    float _randomMousePointerTimer = 0.0f;
    float _randomMovementTimer = 0.0f;
    float _randomShootTimer = 0.0f;
    Vector2 _virtualMousePointer = new Vector2();
    bool _randomMoving = false;
    bool _paused = false;
    bool _title = false;
    bool _gameEnded = false;
    float _gameResetTimer = 0.0f;

    public void Hurt(float damage)
    {
        _hitPoints -= damage;
        _animator.SetTrigger("hurt");
        _audioManager.PlayClip(_tankHitSound);
        Debug.Log("New Health: " + _hitPoints.ToString());
    }

    public bool IsPaused()
    {
        return _paused || _gameEnded;
    }

    // Start is called before the first frame update
    void Start()
    {
        _hitPoints = _maxHitPoints;
        _gameEnded = false;

        if (PlayerController.Instance == null)
        {
            PlayerController.Instance = this;
        }
        else
        {
            GameObject.Destroy(this);
        }

        _audioManager = new AudioManager(this.gameObject);
        _drivingAudioSource = gameObject.AddComponent<AudioSource>();
        _drivingAudioSource.loop = true;

        _rigidBody = GetComponent<Rigidbody2D>();
        _animator = GetComponentInChildren<Animator>();
        _uiController = GetComponentInChildren<UI_Controller>();
        _healthBar = GetComponentInChildren<StatusBar>();

        _uiController.SetWinScreen(false);
        _uiController.SetLoseScreen(false);
        _uiController.SetController(_currentControl);

        showTitle();
    }

    private void showTitle()
    {
        _title = true;
        _paused = true;
        _uiController.SetTitleScreen(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (HandlePause())
        {
            return;
        }

        HandleRandomMousePointer();

        HandleControlSwitch();
        HandleTurret();
        HandleAnimations();
        HandleHealth();
        UpdateCamera();
    }

    private void FixedUpdate()
    {
        HandleMovement();
    }

    bool HandlePause()
    {
        bool escPressed = Input.GetKeyDown(KeyCode.Escape);
        bool primaryPressed = Input.GetButtonDown("PrimaryAction");
        bool qPressed = Input.GetKeyDown(KeyCode.Q);

        if(_gameResetTimer > 0.0f)
        {
            _gameResetTimer -= Time.deltaTime;
        }

        if (_gameEnded)
        {
            if(escPressed || qPressed)
            {
                Application.Quit();
            }
            else if(primaryPressed)
            {
                if (_gameResetTimer <= 0.0f)
                {
                    SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                }
            }
        }
        else
        {
            if (_paused)
            {
                if (escPressed || primaryPressed)
                {
                    if (_title)
                    {
                        _uiController.SetTitleScreen(false);
                        pause();
                        _title = false;
                    }
                    else
                    {
                        unpause();
                    }
                }
                else if(qPressed)
                {
                    Application.Quit();
                }
            }
            else
            {
                if (escPressed)
                {
                    pause();
                }
            }
        }
        return (_paused || _gameEnded);
    }

    void HandleRandomMousePointer()
    {
        if(_randomMousePointerTimer > 0.0f)
        {
            _randomMousePointerTimer -= Time.deltaTime;
        }
        else
        {
            _randomMousePointerTimer = _randomMousePointerTime;
            if(Random.value < _virtualMoustPointerChangeProb)
            {
                float minxVal = 0.0f;
                float maxxVal = _camera.pixelWidth;
                float newXval = Random.Range(minxVal, maxxVal);

                float minyVal = 0.0f;
                float maxyVal = _camera.pixelHeight;
                float newyval = Random.Range(minyVal, maxyVal);

                _virtualMousePointer = _camera.ScreenToWorldPoint(new Vector2(newXval, newyval));
            }
        }
    }

    void HandleHealth()
    {
        _healthBar.FillValue = (_hitPoints / _maxHitPoints);
        if (_hitPoints <= 0)
        {
            // Game over!
            _animator.SetTrigger("death");
            _audioManager.PlayClip(_tankExplosionSound);
            gameLose();
        }
    }

    void HandleAnimations()
    {
        // moving animation
        if (_rigidBody.velocity.magnitude > 0.5f)
        {
            _animator.SetBool("moving", true);

            if (_drivingAudioSource.clip != _drivingSound)
            {
                _drivingAudioSource.Stop();
                _drivingAudioSource.clip = _drivingSound;
                _drivingAudioSource.volume = 1.0f;
                _drivingAudioSource.Play();
            }
            
        }
        else
        {
            _animator.SetBool("moving", false);

            if (_drivingAudioSource.clip != _idleSound)
            {
                _drivingAudioSource.Stop();
                _drivingAudioSource.clip = _idleSound;
                _drivingAudioSource.volume = 0.5f;
                _drivingAudioSource.Play();
            }
        }
    }

    void HandleControlSwitch()
    {
        bool controllerChanged = false;
        // TODO: Handle in more generic way!
        if(Input.GetKeyDown(KeyCode.Alpha2))
        {
            _currentControl = Control.Turret;
            controllerChanged = true;
        }
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            _currentControl = Control.Movement;
            controllerChanged = true;
        }

        if (controllerChanged)
        {
            _uiController.SetController(_currentControl);
            _audioManager.PlayClip(_switchControlSound);
        }

    }

    void UpdateCamera()
    {
        Vector2 vec2tank = transform.position - _camera.transform.position;
        if(vec2tank.magnitude >= 0.0f)
        {
            //Vector2 newCamPos = (Vector2)_camera.transform.position + vec2tank.normalized * 1.0f * Time.deltaTime * vec2tank.magnitude;
            Vector2 newCamPos = (Vector2)transform.position;
            _camera.transform.position = new Vector3(newCamPos.x, newCamPos.y, _camera.transform.position.z);
        }
    }

    void HandleTurret()
    {
        bool shootBullet = false;
        Vector2 mousePos = new Vector2();



        if (_currentControl == Control.Turret)
        {
            mousePos = Input.mousePosition;
            mousePos = _camera.ScreenToWorldPoint(mousePos);
            // shoot bullets
            if(Input.GetButtonDown("PrimaryAction"))
            {
                shootBullet = true;
            }
        }
        else
        {
            mousePos = _virtualMousePointer;
            if (_randomShootTimer > 0.0f)
            {
                _randomShootTimer -= Time.deltaTime;
            }
            else
            {
                _randomShootTimer = _randomShootTime;
                if (Random.value < _randomShootProb)
                {
                    shootBullet = true;
                }
            }
        }

        // rotate turret toward mouse
        rotateObjectToward(_turret.transform, mousePos, _turretRotateSpeed);

        if (_shootTimer <= 0.0f)
        {
            if(shootBullet == true)
            {
                this.shootBullet();
                _shootTimer = _shootDebounce;
            }
        }
        else
        {
            _shootTimer -= Time.deltaTime;
        }
    }

    void HandleMovement()
    {
        Vector2 mousePos;
        bool shouldMove = false;

        if (_currentControl == Control.Movement)
        {
            mousePos = Input.mousePosition;
            mousePos = Camera.main.ScreenToWorldPoint(mousePos);

            if (Input.GetButton("PrimaryAction"))
            {
                shouldMove = true;
            }
        }
        else
        {
            mousePos = _virtualMousePointer;

            if (_randomMovementTimer > 0.0f)
            {
                _randomMovementTimer -= Time.fixedDeltaTime;
            }
            else
            {
                _randomMovementTimer = _randomMovementTime;
                if (Random.value < _randomMovementChangeProb)
                {
                    // toggle movement every now and then
                    _randomMoving = !_randomMoving;
                }
            }

            shouldMove = _randomMoving;
        }

        if(shouldMove)
        {
            float distanceToMouse = (mousePos - (Vector2)transform.position).magnitude;
            // modify speed based on distance of mouse
            float forceModifier = Mathf.Lerp(0.0f, _moveForce, distanceToMouse / 3.0f);
            // tank always goes forward
            Vector2 moveForce = transform.up * _moveForce;
            _rigidBody.AddForce(moveForce);

            // rotate tank toward mouse
            rotateObjectToward(transform, mousePos, _baseRotateSpeed);

            Debug.DrawLine(transform.position, mousePos);
        }
    }

    void OnAnimationTankExplosionFinished()
    {
        _uiController.SetLoseScreen(true);
    }


    void rotateObjectToward(Transform transformToRotate, Vector2 worldLocationToFace, float rotateSpeed)
    {
        Vector2 mouseRel = transformToRotate.InverseTransformPoint(worldLocationToFace);
        float tolerence = 0.5f;
        float rotateSpeedMod = 0;
        if (mouseRel.x > tolerence)
        {
            rotateSpeedMod = -rotateSpeed;
        }
        else if (mouseRel.x < -tolerence)
        {
            rotateSpeedMod = rotateSpeed;
        }
        transformToRotate.Rotate(new Vector3(0, 0, rotateSpeedMod * Time.fixedDeltaTime));
    }

    private void shootBullet()
    {
        GameObject bullet = GameObject.Instantiate(_bulletPrefab,
                                                            new Vector3(_barrelExit.transform.position.x, _barrelExit.transform.position.y, _barrelExit.transform.position.z + 0.5f),
                                                            _turret.transform.rotation);
        _animator.SetTrigger("shoot");
        _audioManager.PlayClip(_shootSound);

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.transform.tag == "EndGoal")
        {
            Debug.Log("Game won!");
            gameWin();
        }
    }

    private void gameWin()
    {
        _gameEnded = true;
        _uiController.SetWinScreen(true);
        _gameResetTimer = 1.0f;
        _drivingAudioSource.Stop();
        _drivingAudioSource.clip = null;
    }

    private void gameLose()
    {
        _gameEnded = true;
        _gameResetTimer = 1.0f;
        _drivingAudioSource.Stop();
        _drivingAudioSource.clip = null;
    }

    private void pause()
    {
        _uiController.SetPauseScreen(true);
        _paused = true;
        Time.timeScale = 0;
    }

    private void unpause()
    {
        _uiController.SetPauseScreen(false);
        _paused = false;
        Time.timeScale = 1;
    }
}
