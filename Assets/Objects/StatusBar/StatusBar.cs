﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[ExecuteInEditMode]
public class StatusBar : MonoBehaviour
{
    /**** Public Properties ****/
    public float FillValue
    {
        get { return _fillValue; }
        set { setFill(value); }
    }

    /**** Editor Exposed values ****/
    [Tooltip("Fill percentage of bar")]
    [SerializeField]
    private float _fillValue;


    /**** My objects ****/
    private GameObject _myBarFiller;


    /**** Unity call backs ****/
    // Start is called before the first frame update
    void Start()
    {
        initiateBarFiller();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // Called on change of any properties in the editor
    private void OnValidate()
    {
        if(_myBarFiller == null)
        {
            initiateBarFiller();
        }
        setFill(_fillValue);
    }

    /**** Private methods ****/
    /// <summary>
    /// Set fill percentage of status bar (of scale between 0.0 and 1.0)
    /// </summary>
    /// <param name="percentage">Fill percentage of scale between 0.0 and 1.0</param>
    private void setFill(float percentage)
    {
        if (percentage > 1.0f)
        {
            percentage = 1.0f;
        }
        else if(percentage < 0.0f)
        {
            percentage = 0.0f;
        }

        _fillValue = percentage;

        Vector3 newScale = _myBarFiller.transform.localScale;
        newScale.x = _fillValue;
        _myBarFiller.transform.localScale = newScale;

        Color barFillerColor = Color.green;
        if(percentage < 0.35f)
        {
            barFillerColor = Color.red;
        }
        else if(percentage < 0.65f)
        {
            barFillerColor = Color.yellow;
        }
        _myBarFiller.GetComponentInChildren<SpriteRenderer>().color = barFillerColor;
    }

    private void initiateBarFiller()
    {
        _myBarFiller = transform.Find("BarFiller").gameObject;
        Debug.AssertFormat(_myBarFiller != null, gameObject.name.ToString() + " does not have object named 'BarFiller' as child!");
    }


}
